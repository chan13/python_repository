#-*- coding: utf-8 -*-

import struct
import numpy
import os, sys, time, subprocess, intelhex

from subprocess import Popen, PIPE
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, \
    QPushButton, QHBoxLayout, QVBoxLayout, QLabel, QComboBox,   \
    QGridLayout, QGroupBox, QCheckBox, QFileDialog, QTextEdit,  \
    QMessageBox, QProgressBar
from PyQt5.QtCore import Qt, QCoreApplication, QBasicTimer
from PyQt5.QtGui import *

from bitstring import BitArray
from optparse import OptionParser
from PIL import Image


class ImageConverter_Qt(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        self.initGlobalVariables()

        self.setWindowTitle('Image Converter & Compress')
        self.setGeometry(300, 200, 400, 350)

        ### grid
        grid = QGridLayout()
        grid.addWidget(self.createCompressCheckboxGroup(),  0, 0)
        grid.addWidget(self.createFileDirSelectGroup(),     1, 0)
        grid.addWidget(self.createColorSizeSelectGroup,     2, 0)
        grid.addWidget(self.createControlButtonGroup(),     3, 0)

        self.setLayout(grid)
        self.show()


    ### 0. init
    def initGlobalVariables(self):
        self.isFilePathComplete = False
        self.isColorComplete = False
        self.isSizeComplete = False
        self.isRotationComplete = False
        self.isCheckedCompress = False


    ### 1. checkbox (compress enable)
    def createCompressCheckboxGroup(self):
        groupbox = QGroupBox()

        checkbox = QCheckBox('Compress ( Logo image )')
        checkbox.setChecked(False)
        checkbox.stateChanged.connect(self.setCompress)

        vbox = QVBoxLayout()
        vbox.addWidget(checkbox)

        groupbox.setLayout(vbox)

        return groupbox

    def setCompress(self, state):
        if state == Qt.Checked:
            self.isCheckedCompress = True
            print('Compress enable')
        else:
            self.isCheckedCompress = False
            print('Compress disable')

        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)


    ### 2. set directory (in/out file)
    def createFileDirSelectGroup(self):
        groupbox = QGroupBox()

        self.textEdit = QTextEdit()

        lbl_1 = QLabel('Input file ( *.bmp )')
        lbl_2 = QLabel('Output file ( *.bin )')

        btn_1 = QPushButton('...')
        btn_1.clicked.connect(self.pushButtonClicked_inDirectory)
        self.lbl_path_in = QLabel(self)
        self.lbl_path_out = QLabel(self)

        hbox = QHBoxLayout()
        hbox.addWidget(lbl_1)
        hbox.addStretch(1)
        hbox.addWidget(btn_1)

        vbox = QVBoxLayout()
        vbox.addLayout(hbox)
        vbox.addWidget(self.lbl_path_in)
        vbox.addSpacing(16)

        hbox = QHBoxLayout()
        hbox.addWidget(lbl_2)
        hbox.addStretch(1)

        vbox.addLayout(hbox)
        vbox.addWidget(self.lbl_path_out)
        vbox.addSpacing(16)

        groupbox.setLayout(vbox)

        return groupbox

    def pushButtonClicked_inDirectory(self):
        fname = QFileDialog.getOpenFileName(self)

        if fname[0]:
            if fname[0].find(".bmp") != -1:
                self.isFilePathComplete = True

                tmpStr = fname[0]
                self.path_in = tmpStr.replace("/", "\\")
                self.lbl_path_in.setText(self.path_in)

                tmpStr = fname[0].replace(".bmp", ".bin")
                self.path_out = tmpStr.replace("/", "\\")
                self.lbl_path_out.setText(self.path_out)

            else:
                self.isFilePathComplete = False

                errorMsgBox = QMessageBox.critical(
                    self, 'error', "please check input file format (*.bmp)",
                    QMessageBox.Close
                )

                if errorMsgBox == QMessageBox.Close:
                    print('Close clicked')
        else:
            print("no file")

        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)

        # print('filepath:', fname[0])

        # if fname[0]:
        #     f = open(fname[0], 'r')
        #
        #     with f:
        #         data = f.read()
        #         self.textEdit.setText(data)


    ### 3. set color, size
    @property
    def createColorSizeSelectGroup(self):
        groupbox = QGroupBox()

        ### Combo box (color)
        # self.lbl_color = QLabel('Color', self)
        lbl_color = QLabel('Color', self)
        self.lbl_selectedColor = QLabel(self)

        cb_color = QComboBox(self)
        cb_color.addItem('            ')
        cb_color.addItem('2C')
        cb_color.addItem('3CR')
        cb_color.addItem('3CY')
        cb_color.addItem('4C')
        cb_color.addItem('7C')

        cb_color.activated[str].connect(self.getSelectedColor)

        ### Combo box (size)
        # self.lbl_size = QLabel('Size', self)
        lbl_size = QLabel('Size', self)
        self.lbl_selectedSize = QLabel(self)

        self.cb_size = QComboBox(self)
        self.cb_size.addItem('            ')

        self.cb_size.activated[str].connect(self.getSelectedSize)

        ### Combo box (rotation)
        lbl_rotation = QLabel('Rotation', self)
        self.lbl_selectedRotation = QLabel(self)

        self.cb_rotation = QComboBox(self)
        self.cb_rotation.addItem('            ')

        self.cb_rotation.activated[str].connect(self.getSelectedRotation)

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(lbl_color)
        hbox.addWidget(cb_color)
        hbox.addStretch(2)
        hbox.addWidget(lbl_size)
        hbox.addWidget(self.cb_size)
        hbox.addStretch(2)
        hbox.addWidget(lbl_rotation)
        hbox.addWidget(self.cb_rotation)
        hbox.addStretch(1)

        groupbox.setLayout(hbox)

        return groupbox

    def getSelectedColor(self, text):
        self.lbl_selectedColor = text
        print("color: ",self.lbl_selectedColor)
        # self.lbl_selectedColor.setText(text)
        # self.lbl_selectedColor.adjustSize()
        # print(text)

        self.cb_size.clear()
        self.cb_size.addItem('            ')

        self.cb_rotation.clear()
        self.cb_rotation.addItem('            ')

        if self.lbl_selectedColor != '            ':
            self.isColorComplete = True

            # update size
            if text == '4C':
                self.cb_size.addItem('1.6')
                self.cb_size.addItem('2.4')
                self.cb_size.addItem('3.0')
                self.cb_size.addItem('4.4')
                self.cb_size.addItem('7.3')
                self.cb_size.addItem('8.2')
            elif text == '7C':
                self.cb_size.addItem('6.0')
            else:
                self.cb_size.addItem('1.3')
                self.cb_size.addItem('1.6')
                self.cb_size.addItem('2.2')
                self.cb_size.addItem('2.6')
                self.cb_size.addItem('2.7')
                self.cb_size.addItem('2.9')
                self.cb_size.addItem('3.52')
                self.cb_size.addItem('3.7')
                self.cb_size.addItem('4.2')
                self.cb_size.addItem('4.3')
                self.cb_size.addItem('5.85')
                self.cb_size.addItem('6.0')
                self.cb_size.addItem('7.5')
                self.cb_size.addItem('9.7')
                self.cb_size.addItem('11.6')
                self.cb_size.addItem('12.2')
                self.cb_size.addItem('13.3')

            # update rotation
            self.cb_rotation.addItem('0')
            self.cb_rotation.addItem('90')
            self.cb_rotation.addItem('180')
            self.cb_rotation.addItem('270')

        else:
            self.isColorComplete = False
            self.isSizeComplete = False
            self.isRotationComplete = False

        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)

    def getSelectedSize(self, text):
        self.lbl_selectedSize = text
        print("size: ",self.lbl_selectedSize)

        if self.lbl_selectedSize != '            ':
            self.isSizeComplete = True
            self.isRotationComplete = True

            if (text == '1.6') or (text == '4.2') or (text == '4.3') or (text == '4.4') \
                    or (text == '6.0'):
                self.cb_rotation.setCurrentIndex(1)     # rotation '0'
            else :
                self.cb_rotation.setCurrentIndex(4)     # rotation '270'

            self.lbl_selectedRotation = self.cb_rotation.currentText()
            print("rotation: ", self.lbl_selectedRotation)

        else:
            self.isSizeComplete = False

        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)

    def getSelectedRotation(self, text):
        self.lbl_selectedRotation = text
        print("rotation: ",self.lbl_selectedRotation)

        if self.lbl_selectedRotation != '            ':
            self.isRotationComplete = True
        else:
            self.isRotationComplete = False

        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)

    ### 4. start/cancel button
    def createControlButtonGroup(self):
        groupbox = QGroupBox()

        ### Progress Bar
        self.pBar = QProgressBar(self)
        self.pBar.setAlignment(Qt.AlignCenter)

        self.timer = QBasicTimer()
        self.pBarStep = 0
        self.pBar.setValue(self.pBarStep)

        ### Button
        Btn_start = QPushButton('Start')
        Btn_cancel = QPushButton('Cancel')

        Btn_start.clicked.connect(self.eventStartButtonClicked)
        Btn_cancel.clicked.connect(QCoreApplication.instance().quit)

        ### Layout
        hbox = QHBoxLayout()
        hbox.addStretch(4)
        hbox.addWidget(Btn_start)
        hbox.addStretch(1)
        hbox.addWidget(Btn_cancel)
        hbox.addStretch(4)

        vbox = QVBoxLayout()
        vbox.addStretch(1)
        vbox.addWidget(self.pBar)
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        groupbox.setLayout(vbox)

        return groupbox

    def eventStartButtonClicked(self):
        print('start button clicked')

        if self.isFilePathComplete == False \
            or self.isColorComplete == False \
            or self.isRotationComplete == False:

            print('[error] parameter setting')

            errorMsgBox = QMessageBox.critical(
                self, 'error', "Please check parameter",
                QMessageBox.Close
            )
            if errorMsgBox == QMessageBox.Close:
                print('Close clicked')

        else:
            self.pBarStep = 0
            self.pBar.setValue(self.pBarStep)

            self.timer.start(20, self)     # milliseconds

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            self.startImageConvert()

            self.pBarStep += 10
            self.pBar.setValue(self.pBarStep)

            if self.isCheckedCompress == True:
                self.startCompress()
                self.convertCompFile_BinToHex()

            self.pBarStep = 100
            self.pBar.setValue(self.pBarStep)

    def timerEvent(self, e):
        if self.pBarStep >= 100:
            self.timer.stop()
            return

        self.pBarStep = self.pBarStep + 1
        self.pBar.setValue(self.pBarStep)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:        # esc key
            self.close()

    def startImageConvert(self):

        path = os.getcwd()
        infile = self.path_in
        outfile = self.path_out
        threshold = 128
        color = self.lbl_selectedColor
        size = self.lbl_selectedSize
        rotation = int(self.lbl_selectedRotation)

        if (color == '4c') or (color == '4C'):

            ########## 1. START IMAGE LOAD ##########
            im = Image.open(os.path.join(path, infile).replace('\\', r'\\'))
            im = im.transpose(Image.FLIP_TOP_BOTTOM)

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            ########## 2. Seperate BLACK & RED & YELLOW IMAGE ##########
            # Make CANVAS
            width, height = im.size
            im_KW = Image.new("RGB", (width, height), (255, 255, 255))
            im_R = Image.new("RGB", (width, height), (255, 255, 255))
            im_Y = Image.new("RGB", (width, height), (255, 255, 255))
            # Make KW Image : output Buffer : im_KW
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r < 128) and (g < 128) and (b < 128):  # KW
                        im_KW.putpixel((j, i), (0, 0, 0))
                    else:
                        im_KW.putpixel((j, i), (255, 255, 255))

            # Make R Image : output Buffer : im_R
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r > 128) and (g < 128) and (b < 128):  # RED
                        im_R.putpixel((j, i), (255, 0, 0))
                    else:
                        im_R.putpixel((j, i), (255, 255, 255))

            # Make Y Image : output Buffer : im_Y
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r > 127) and (g > 127) and (b < 128):  # Yellow
                        im_Y.putpixel((j, i), (255, 0, 0))
                    else:
                        im_Y.putpixel((j, i), (255, 255, 255))

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            ########## 3. Convert Color, Rotate Image ##########
            # for Header
            im_KWRY = im.convert('1')
            im_KWRY.save('Remove_Temp.bmp')
            buf_KWRY = ''
            # for KW Left
            im_KW = im_KW.convert('L').point(lambda p: p < threshold)
            im_KW = im_KW.rotate(rotation, expand=1)
            width, height = im_KW.size
            buf_KW = ''

            # for R Left
            im_R = im_R.convert('L').point(lambda p: p < threshold)
            im_R = im_R.rotate(rotation, expand=1)
            width, height = im_R.size
            buf_R = ''

            # for Y Left
            im_Y = im_Y.convert('L').point(lambda p: p < threshold)
            im_Y = im_Y.rotate(rotation, expand=1)
            width, height = im_Y.size
            buf_Y = ''

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            ########## 4. Make Input Binary ##########
            # KW : output Buffer : buf_KW
            print(width, height)
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_KW.getpixel((i, j)))

                    if (val == '0'):
                        bit += '00'
                    else:
                        val = str(1 - im_R.getpixel((i, j)))

                        if (val == '0'):
                            bit += '11'

                        else:
                            val = str(1 - im_Y.getpixel((i, j)))

                            if (val == '0'):
                                bit += '10'

                            else:
                                bit += '01'

                    if ((i + 1) % 4) == 0:
                        buf_KWRY += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            ########## 5. Merge Final Input Binary ##########
            # Header Extract

            im_KWRY_bin = open('Remove_Temp.bmp', 'rb')
            # im_KWRY_bin_data = im_KWRY_bin.readline()
            im_KWRY_bin_data = []
            im_KWRY_bin_data = im_KWRY_bin.read()
            width, height = im_KWRY.size
            print("4C SIZE", width, height)

            im_KWRY_bin.close()
            os.remove('./Remove_Temp.bmp')

            bmp_header = 62
            KWRY_len = width * height // 4  # 8 pixel 1 byte, 2 Panel (KW, R, Y)
            output_buffer = []  # [0] * KWRY_len + [0] * bmp_header
            output_buffer_save = []  # [0] * KWRY_len + [0] * bmp_header

            # Copy Header
            for i in range(0, bmp_header):
                output_buffer.append(format(im_KWRY_bin_data[i], '02x'))

            convert_len = (width * height) // 4  # 8 pixel 1 byte
            temp_buffer = []  # [0] * convert_len

            # Append KW
            index = bmp_header

            print(len(buf_KWRY), width, height, convert_len, KWRY_len)

            for i in range(0, convert_len):
                buf_temp = buf_KWRY[i * 2] + buf_KWRY[i * 2 + 1]
                output_buffer.insert(index + i, buf_temp)
                # output_buffer[index + i] = buf_temp

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

            file = open(outfile, 'wb')

            for i in range(0, KWRY_len + bmp_header):
                file.write(bytes.fromhex(output_buffer[i]))

            file.close()

            self.pBarStep += 5
            self.pBar.setValue(self.pBarStep)

        elif (color == '7c') or (color == '7C'):  # 7 color
            ########## 1. START IMAGE LOAD ##########
            im = Image.open(os.path.join(path, infile).replace('\\', r'\\'))
            im = im.transpose(Image.FLIP_TOP_BOTTOM)

            ########## 2. Seperate BLACK & RED & YELLOW IMAGE ##########
            # Make CANVAS
            width, height = im.size
            im_sevenColor = Image.new("RGB", (width, height), (255, 255, 255))
            buf_7C = ''
            val = ''

            # Make KW Image : output Buffer : im_KW
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r < 128) and (g < 128) and (b < 128):  # BLACK
                        im_sevenColor.putpixel((j, i), (0, 0, 0))
                        val += '0'
                    elif (r > 128) and (g < 128) and (b < 128):  # RED
                        im_sevenColor.putpixel((j, i), (255, 0, 0))
                        val += '4'
                    elif (r > 127) and (g > 127) and (b < 128):  # Yellow
                        im_sevenColor.putpixel((j, i), (255, 255, 0))
                        val += '5'
                    elif (r < 129) and (g < 256) and (b < 155):  # GREEN
                        im_sevenColor.putpixel((j, i), (0, 128, 0))
                        val += '2'
                    elif (r < 176) and (g < 256) and (b > 128):  # BLUE
                        im_sevenColor.putpixel((j, i), (0, 0, 255))
                        val += '3'
                    elif (r == 255) and (g < 166) and (b < 140):  # ORANGE
                        im_sevenColor.putpixel((j, i), (255, 165, 0))
                        val += '6'
                    else:
                        im_sevenColor.putpixel((j, i), (255, 255, 255))  # WHITE
                        val += '1'

                    if ((j + 1) % 2) == 0:
                        buf_7C += val
                        val = ''
            ########## 3. Convert Color, Rotate Image ##########
            # for Header
            im_7C_Total = im.convert('1')
            im_7C_Total.save('Remove_Temp.bmp')

            # for KW Left
            # im_sevenColor = im_sevenColor.convert('L').point(lambda p: p < threshold)
            # if (size == '5.8') or (size == '7.5') or (size == '2.7') or (size == '2.6') or (size == '13.3'):
            #     im_sevenColor = im_sevenColor.rotate(270, expand=1)
            # elif (size != '1.6') and (size != '6.0') and (size != '7.4'):
            #     im_sevenColor = im_sevenColor.rotate(90, expand=1)

            width, height = im_sevenColor.size
            '''
            buf_7C = ''

            ########## 4. Make Input Binary ##########
            # KW : output Buffer : buf_KW
            print (width, height)

            val = ''
            for j in range(0, height):
                for i in range(0, width):
                    r, g, b = im_sevenColor.getpixel((i, j))

                    if (r == 0) and (g == 0) and (b == 0):  # BLACK
                        val = '0'
                    elif (r == 255) and (g == 0) and (b == 0):  # RED
                        val = '4'
                    elif (r == 255) and (g == 255) and (b == 0):  # Yellow
                        val = '5'
                    elif (r == 0) and (g == 128) and (b == 0):  # GREEN
                        val = '2'
                    elif (r == 0) and (g == 0) and (b == 255):  # BLUE
                        val = '3'
                    elif (r == 255) and (g == 165) and (b == 0):  # ORANGE
                        val = '6'
                    else:
                        val = '1'  # WHITE

                    val += val
                    if ((i + 1) % 2) == 0:
                        buf_7C += val
            '''
            ########## 5. Merge Final Input Binary ##########
            # Header Extract

            im_7C_Total_bin = open('Remove_Temp.bmp', 'rb')
            # im_KWRY_bin_data = im_KWRY_bin.readline()
            im_7C_Total_bin_data = []
            im_7C_Total_bin_data = im_7C_Total_bin.read()
            width, height = im_7C_Total.size
            print("7C SIZE", width, height)

            im_7C_Total_bin.close()
            os.remove('./Remove_Temp.bmp')

            bmp_header = 62
            im_7C_Total_len = width * height // 2  # 2 pixel 1 byte, 2 Panel (KW, R, Y)
            output_buffer = []  # [0] * KWRY_len + [0] * bmp_header
            output_buffer_save = []  # [0] * KWRY_len + [0] * bmp_header

            # Copy Header
            for i in range(0, bmp_header):
                # output_buffer.append(im_7C_Total_bin_data[i].encode("hex"))
                output_buffer.append(format(im_7C_Total_bin_data[i], '02x'))

            convert_len = (width * height) // 2  # 2 pixel 1 byte
            temp_buffer = []  # [0] * convert_len

            # Append KW
            index = bmp_header

            print(len(buf_7C), width, height, convert_len, im_7C_Total_len)

            for i in range(0, convert_len):
                buf_temp = buf_7C[i * 2] + buf_7C[i * 2 + 1]
                output_buffer.insert(index + i, buf_temp)
                # output_buffer[index + i] = buf_temp

            file = open(outfile, 'wb')

            for i in range(0, im_7C_Total_len + bmp_header):
                file.write(bytes.fromhex(output_buffer[i]))

            file.close()

        elif (color == '3cr') or (color == '3CR'):

            ########## 1. START IMAGE LOAD ##########
            im = Image.open(os.path.join(path, infile).replace('\\', r'\\'))
            im = im.transpose(Image.FLIP_TOP_BOTTOM)

            ########## 2. Seperate BLACK & RED IMAGE ##########
            # Make CANVAS
            width, height = im.size
            im_KW = Image.new("RGB", (width, height), (255, 255, 255))
            im_R = Image.new("RGB", (width, height), (255, 255, 255))

            # Make KW Image : output Buffer : im_KW
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r < 128) and (g < 128) and (b < 128):  # KW
                        im_KW.putpixel((j, i), (0, 0, 0))
                    else:
                        im_KW.putpixel((j, i), (255, 255, 255))

            # Make R Image : output Buffer : im_R
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r > 128) and (g < 128) and (b < 128):  # RED
                        im_R.putpixel((j, i), (255, 0, 0))
                    else:
                        im_R.putpixel((j, i), (255, 255, 255))

            ########## 3. Convert Color, Rotate Image ##########
            # for Header
            im_KWR = im.convert('1')
            im_KWR.save('Remove_Temp.bmp')

            # for KW Left
            im_KW = im_KW.convert('L').point(lambda p: p < threshold)

            # if (size == '5.8') or (size == '7.5') or (size == '2.7') or (size == '2.6') or (size == '13.3'):
            #     im_KW = im_KW.rotate(270, expand=1)
            # elif (size != '1.6') and (size != '6.0') and (size != '7.4'):
            #     im_KW = im_KW.rotate(90, expand=1)

            im_KW = im_KW.rotate(rotation, expand=1)
            width, height = im_KW.size
            buf_KW = ''

            # for R Left
            im_R = im_R.convert('L').point(lambda p: p < threshold)

            # if (size == '5.8') or (size == '7.5') or (size == '2.7') or (size == '2.6') or (size == '13.3'):
            #     im_R = im_R.rotate(270, expand=1)
            # elif (size != '1.6') and (size != '6.0') and (size != '7.4'):
            #     im_R = im_R.rotate(90, expand=1)
            im_R = im_R.rotate(rotation, expand=1)
            width, height = im_R.size
            buf_R = ''

            ########## 4. Make Input Binary ##########
            # KW : output Buffer : buf_KW
            print(width, height)
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_KW.getpixel((i, j)))
                    bit += val

                    if ((i + 1) % 8) == 0:
                        buf_KW += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            # R : output Buffer : buf_R
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_R.getpixel((i, j)))
                    bit += val

                    if ((i + 1) % 8) == 0:
                        buf_R += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            ########## 5. Merge Final Input Binary ##########
            # Header Extract
            im_KWR_bin = open('Remove_Temp.bmp', 'rb')
            # im_KWR_bin_data = im_KWR_bin.readline()
            im_KWR_bin_data = []
            im_KWR_bin_data = im_KWR_bin.read()
            width, height = im_KWR.size
            print("3CR SIZE", width, height)
            im_KWR_bin.close()
            os.remove('./Remove_Temp.bmp')

            bmp_header = 62
            KWR_len = width * height * 2 // 8  # 8 pixel 1 byte, 2 Panel (KW, R)
            # output_buffer = [0] * KWR_len + [0] * bmp_header
            # output_buffer_save = [0] * KWR_len + [0] * bmp_header
            output_buffer = []  # [0] * KWRY_len + [0] * bmp_header
            output_buffer_save = []  # [0] * KWRY_len + [0] * bmp_header

            # Copy Header
            for i in range(0, bmp_header):
                # output_buffer.append(im_KWR_bin_data[i])
                # output_buffer[i] = im_KWR_bin_data[i].encode("hex")
                output_buffer.append(format(im_KWR_bin_data[i], '02x'))

            convert_len = (width * height) // 8  # 8 pixel 1 byte
            temp_buffer = [0] * convert_len

            # Append KW
            index = bmp_header

            print(len(buf_KW), len(buf_R), width, height, convert_len, KWR_len)

            for i in range(0, convert_len):
                buf_temp = buf_KW[i * 2] + buf_KW[i * 2 + 1]
                output_buffer.insert(index + i, buf_temp)
                # output_buffer[index + i] = buf_temp

            # Append R
            index += convert_len

            for i in range(0, convert_len):
                buf_temp = buf_R[i * 2] + buf_R[i * 2 + 1]
                output_buffer.insert(index + i, buf_temp)
                # output_buffer[index + i] = buf_temp

            file = open(outfile, 'wb')

            for i in range(0, KWR_len + bmp_header):
                file.write(bytes.fromhex(output_buffer[i]))

            file.close()

        elif (color == '3cy') or (color == '3CY'):

            ########## 1. START IMAGE LOAD ##########
            im = Image.open(os.path.join(path, infile).replace('\\', r'\\'))
            # if (size == '1.6') or (size == '6.0') or (size == '7.4') or (size == '2.6') or (size == '2.7') or (
            #         size == '7.5') or (size == '5.8') or (size == '13.3'):
            #     # im = im.transpose(Image.FLIP_LEFT_RIGHT)
            #     im = im.transpose(Image.FLIP_TOP_BOTTOM)

            # im = im.transpose(Image.FLIP_LEFT_RIGHT)
            im = im.transpose(Image.FLIP_TOP_BOTTOM)

            ########## 2. Seperate BLACK & YELLOW IMAGE ##########
            # Make CANVAS
            width, height = im.size
            im_KW = Image.new("RGB", (width, height), (255, 255, 255))
            im_Y = Image.new("RGB", (width, height), (255, 255, 255))

            # Make KW Image : output Buffer : im_KW
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r < 128) and (g < 128) and (b < 128):  # KW
                        im_KW.putpixel((j, i), (0, 0, 0))
                    else:
                        im_KW.putpixel((j, i), (255, 255, 255))

            # Make Y Image : output Buffer : im_Y
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r > 127) and (g > 127) and (b < 128):  # YELLOW
                        im_Y.putpixel((j, i), (255, 0, 0))
                    else:
                        im_Y.putpixel((j, i), (255, 255, 255))

            ########## 3. Convert Color, Rotate Image ##########
            # for Header
            im_KWY = im.convert('1')
            im_KWY.save('Remove_Temp.bmp')

            # for KW Left
            im_KW = im_KW.convert('L').point(lambda p: p < threshold)
            im_KW = im_KW.rotate(rotation, expand=1)
            width, height = im_KW.size
            buf_KW = ''

            # for Y Left
            im_Y = im_Y.convert('L').point(lambda p: p < threshold)
            im_Y = im_Y.rotate(rotation, expand=1)
            width, height = im_Y.size
            buf_Y = ''

            ########## 4. Make Input Binary ##########
            # KW : output Buffer : buf_KW
            print(width, height)
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_KW.getpixel((i, j)))
                    bit += val

                    if ((i + 1) % 8) == 0:
                        buf_KW += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            # R : output Buffer : buf_R
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_Y.getpixel((i, j)))
                    bit += val

                    if ((i + 1) % 8) == 0:
                        buf_Y += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            ########## 5. Merge Final Input Binary ##########
            # Header Extract

            im_KWY_bin = open('Remove_Temp.bmp', 'rb')
            im_KWY_bin_data = im_KWY_bin.readline()
            width, height = im_KWY.size
            print("3CY SIZE", width, height)
            im_KWY_bin.close()
            os.remove('./Remove_Temp.bmp')

            bmp_header = 62
            KWY_len = width * height * 2 / 8  # 8 pixel 1 byte, 2 Panel (KW, R)
            # output_buffer = [0] * KWY_len + [0] * bmp_header
            # output_buffer_save = [0] * KWY_len + [0] * bmp_header
            output_buffer = []  # [0] * KWRY_len + [0] * bmp_header
            output_buffer_save = []  # [0] * KWRY_len + [0] * bmp_header

            # Copy Header
            for i in range(0, bmp_header):
                output_buffer.append(format(im_KWY_bin_data[i], '02x'))

            convert_len = (width * height) / 8  # 8 pixel 1 byte
            temp_buffer = [0] * convert_len

            # Append KW
            index = bmp_header

            print(len(buf_KW), len(buf_Y), width, height, convert_len)

            for i in range(0, convert_len):
                buf_temp = buf_KW[i * 2] + buf_KW[i * 2 + 1]
                output_buffer[index + i] = buf_temp

            # Append R
            index += convert_len

            for i in range(0, convert_len):
                buf_temp = buf_Y[i * 2] + buf_Y[i * 2 + 1]
                output_buffer[index + i] = buf_temp

            file = open(outfile, 'wb')

            for i in range(0, KWY_len + bmp_header):
                file.write(bytes.fromhex(output_buffer[i]))

            file.close()

        elif (color == '2c') or (color == '2C'):
            ########## 1. START IMAGE LOAD ##########
            im = Image.open(os.path.join(path, infile).replace('\\', r'\\'))
            im = im.transpose(Image.FLIP_TOP_BOTTOM)

            ########## 2. Seperate BLACK & RED IMAGE ##########
            # Make CANVAS
            width, height = im.size
            im_KW = Image.new("RGB", (width, height), (255, 255, 255))

            # Make KW Image : output Buffer : im_KW
            for i in range(0, height):
                for j in range(0, width):
                    r, g, b = im.getpixel((j, i))

                    if (r < 128) and (g < 128) and (b < 128):  # KW
                        im_KW.putpixel((j, i), (0, 0, 0))
                    else:
                        im_KW.putpixel((j, i), (255, 255, 255))

            ########## 3. Convert Color, Rotate Image ##########
            # for Header
            im_KWR = im.convert('1')
            im_KWR.save('Remove_Temp.bmp')

            # for KW Left
            im_KW = im_KW.convert('L').point(lambda p: p < threshold)
            im_KW = im_KW.rotate(rotation, expand=1)
            width, height = im_KW.size
            buf_KW = ''

            ########## 4. Make Input Binary ##########
            # KW : output Buffer : buf_KW
            print(width, height)
            bit = ''
            for j in range(0, height):
                for i in range(0, width):
                    val = str(1 - im_KW.getpixel((i, j)))
                    bit += val

                    if ((i + 1) % 8) == 0:
                        buf_KW += BitArray(bin=bit).hex
                        bit = ''

                bit = ''

            ########## 5. Merge Final Input Binary ##########
            # Header Extract
            im_KWR_bin = open('Remove_Temp.bmp', 'rb')
            im_KWR_bin_data = im_KWR_bin.readline()
            width, height = im_KWR.size
            print("2C SIZE", width, height)
            im_KWR_bin.close()
            os.remove('./Remove_Temp.bmp')

            bmp_header = 62
            KWR_len = width * height / 8  # 8 pixel 1 byte, 2 Panel (KW, R)
            # output_buffer = [0] * KWR_len + [0] * bmp_header
            # output_buffer_save = [0] * KWR_len + [0] * bmp_header
            output_buffer = []  # [0] * KWRY_len + [0] * bmp_header
            output_buffer_save = []  # [0] * KWRY_len + [0] * bmp_header

            # Copy Header
            for i in range(0, bmp_header):
                # output_buffer[i] = im_KWR_bin_data[i].encode("hex")
                output_buffer.append(format(im_KWR_bin_data[i], '02x'))

            convert_len = (width * height) / 8  # 8 pixel 1 byte
            temp_buffer = [0] * convert_len

            # Append KW
            index = bmp_header

            print(len(buf_KW), width, height, convert_len)

            for i in range(0, convert_len):
                buf_temp = buf_KW[i * 2] + buf_KW[i * 2 + 1]
                output_buffer[index + i] = buf_temp

            file = open(outfile, 'wb')

            for i in range(0, KWR_len + bmp_header):
                file.write(bytes.fromhex(output_buffer[i]))

            file.close()

        else:
            print('Invalid Color Information')

    def startCompress(self):
        print("Start Compress")

        # self.path_out = ("D:\\00.ws_chan\\03.pycharm_project\\ImageConverter_Comp\\N-System_M3_1P6(168x168)_BWRY_ID_Font.bin")
        self.path_out_comp = self.path_out.replace('.bin', '_comp.bin')

        subprocess.call(['.\\CompDecompPNG.exe', '-l10', 'c', self.path_out, self.path_out_comp])

        print("Finished Compress")

    def convertCompFile_BinToHex(self):
        ### 1. open binary file ("rb": read binary)
        f_comp = open(self.path_out_comp, "rb")
        # f_comp = open(".\\N-System_M3_8P2(1024x576)_BWRY_ID_Font_rot90_comp.bin", "rb")

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)

        ### 2. get file size
        f_comp.seek(0, os.SEEK_END)         # move file cursor to end
        fileSize = f_comp.tell()            # get the current cursor position
        print('Size of comp file(read) is', fileSize, 'bytes')
        f_comp.seek(0, os.SEEK_SET)  # move file cursor to end

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)

        ### 3. make dataset (padding + fileSize + data)
        # set padding bytes (255 bytes)(0x00~0xFF : '0xFF')
        paddingList = []
        for i in range(0x100):
            paddingList.append(0xFF)

        # set file size (4bytes)
        sizeList = []
        sizeList.append(fileSize & 0x000000FF)
        sizeList.append((fileSize & 0x0000FF00) >> 8)
        sizeList.append((fileSize & 0x00FF0000) >> 16)
        sizeList.append((fileSize & 0xFF000000) >> 24)

        # get data from file (fileSize bytes)
        readList = list(f_comp.read((fileSize)))
        f_comp.close()

        # merge all dataset
        dataList = paddingList + sizeList + readList
        # for i in range(len(dataList)):
        #     # dataList[i] = hex(dataList[i])
        #     print("[", hex(i), "]: ", dataList[i])

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)

        ### 4. make comp file with header
        dataListArray = bytearray(dataList)
        self.path_out_comp = self.path_out_comp.replace('_comp.bin', '_compWithHeader.bin')
        f_compWithHeader = open(self.path_out_comp, "wb")
        # f_compWithHeader = open(".\\N-System_M3_8P2(1024x576)_BWRY_ID_Font_rot90_compWithHeader.bin", "wb")
        f_compWithHeader.write((dataListArray))
        f_compWithHeader.close()

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)

        ### 5. comvert file (bin -> intel hex) (only LOGO image : 0x0002C000)
        ih = intelhex.IntelHex()
        ih.loadbin(self.path_out_comp)
        # ih.loadbin('.\\N-System_M3_8P2(1024x576)_BWRY_ID_Font_rot90_compWithHeader.bin')

        # ih.start_addr = { 'CS' : 0x0002 , 'IP' : 0xC000 }
        ih.start_addr = { 'EIP' : 0x0002 }

        self.path_out_comp = self.path_out_comp.replace('.bin', '.hex')
        f_outputHex = open(self.path_out_comp, 'w')
        # f_outputHex = open(".\\N-System_M3_8P2(1024x576)_BWRY_ID_Font_rot90_compAndHeader.hex", "w")

        ih.tofile(f_outputHex, "hex")
        f_outputHex.close()

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)

        ### 6. remove tmp file
        # self.path_out_comp = self.path_out.replace('.hex', '.bin')
        # os.remove(self.path_out_comp)
        # os.remove(".\\N-System_M3_8P2(1024x576)_BWRY_ID_Font_rot90_compAndHeader.bin")

        self.pBarStep += 5
        self.pBar.setValue(self.pBarStep)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ImageConverter_Qt()
    sys.exit(app.exec_())