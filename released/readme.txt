
***************************************
* Image converter & compress GUI tool *
***************************************
---------------------------------------
* Release
---------------------------------------
  [v4] 210908
	- Add new parameter 'Rotation'
	  1. color & rotation 선택하여 변환 가능
	  2. color & size 선택 시 해당 rotation 자동 선택
	- (3C) 4.2" 오동작 버그 해결

  [v3] 210803
	- (4C) 7.3" 신규패널 추가
	- (4C) 8.2" rotation 수정 (0 -> 270)

  [v2] 210602
	- (3CR, 3CY, 2C)size별 Tag 목록 업데이트
	- 3CR L-R Flip 제거

  [v1] 210602, 최초버전
	- Image conver & Compress
	- (4C) 출력물 확인 완료

---------------------------------------
* Feature
---------------------------------------
  - Sequence
	1. *.bmp -> *.bin 		(Image convert)
	2. *.bin -> *_comp.bin 		(compress)
	3. *_comp.bin -> *_compWithHeader.bin
	4. *_compWithHeader.bin -> *_compWithHeader.hex

---------------------------------------
* Manual
---------------------------------------
  - Parameters
  	1. Image file (*.bmp)
  	2. Tag type (color, size)
  - 'CompDecompPNG.exe' 파일과 동일 디렉토리에 위치

  - pyinstaller
	1. 실행파일 생성
	  pyinstaller -w -F ImageConverterAndCompress.py
	  -w : 콘솔창 출력 x
	  -F : 실행파일 하나만 생성 (= '-onefile')

---------------------------------------
* TODO
---------------------------------------